#include "MacTableThread.h"

using Tins::PDU;

MacTableThread::MacTableThread(QObject *parent)
    : QThread(parent)
{
    m_timer = 30;
}

MacTableThread::~MacTableThread()
{
    mutex.lock();
    condition.wakeOne();
    mutex.unlock();

    wait();
}

void MacTableThread::find_entry(const QString &address, const QString &port) {
    bool found = false;
    for (int i = 0; i < rows.size(); i++) {
        MacRow *row = rows.at(i);
        if (!row->address().compare(address)) {
            found = true;
            if (!row->port().compare(port)) {
                row->reset_age();
                return;
            } else {
                remove(i);
                add(address, port);
            }
        }
    }
    if (found == false) {
        add(address, port);
    }
}

QString MacTableThread::find_dest(const QString &dst_mac) const
{
    foreach ( MacRow *row, rows ) {
        if (!row->address().compare(dst_mac)) {
            return row->port();
        }
    }
    return NULL;
}

void MacTableThread::add(const QString &address, const QString &port)
{
    MacRow *row = new MacRow(address, port);
    mutex.lock();
    rows.append( row );
    mutex.unlock();
    emit display_row(address, port);
}

void MacTableThread::run()
{
    while(true) {
        for (int i = 0; i < rows.size(); i++) {
            MacRow *row = rows.at(i);
            if (row->elapsed() > m_timer) {
                remove(i);
            }
        }
    }
}

void MacTableThread::remove(int idx)
{
    mutex.lock();
    rows.removeAt(idx);
    mutex.unlock();
    emit hide_row(idx);
}

void MacTableThread::clear()
{
    mutex.lock();
    rows.clear();
    mutex.unlock();
    emit flush_mac_table();
}

int MacTableThread::timer()
{
    return m_timer;
}

void MacTableThread::set_mac_timer(int seconds)
{
    mutex.lock();
    m_timer = seconds;
    mutex.unlock();
}
