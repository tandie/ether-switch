#ifndef PORTLISTTHREAD_H
#define PORTLISTTHREAD_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <tins/network_interface.h>
#include <tins/pdu.h>
#include <iostream>

#include <PortThread.h>
#include <MacTableThread.h>
#include <iostream>

using Tins::NetworkInterface;
using Tins::PDU;

class PortListThread : public QThread
{
    Q_OBJECT

public:
    PortListThread(QObject *parent = 0);
    ~PortListThread();

    QList<PortThread *> get_ports() const;
    PortThread* find(const QString &port_name);
    void refresh();
    void unicast(const QString &dst_port, PDU &pdu);
    void broadcast(const QString &src_port, PDU &pdu);

signals:
    QString find_dest(const QString &dst_mac) const;
    void display_port(const QString &name, const bool &active);
    void port_down(const int idx);
    void port_up(const int idx);

protected:
    void run();

private:
    QMutex mutex;
    QWaitCondition condition;
    QList<PortThread *> ports;
};

#endif // PORTLISTTHREAD_H
