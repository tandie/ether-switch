#include "MacRow.h"


MacRow::MacRow()
{
    m_age.start();
}

MacRow::MacRow(const MacRow &other)
{
    m_address = other.address();
    m_port = other.port();
    m_age = other.age();
}

MacRow::~MacRow()
{
}

MacRow::MacRow(const QString &address, const QString &port)
{
    m_address = address;
    m_port = port;
    m_age.start();
}

QString MacRow::port() const
{
    return m_port;
}

QString MacRow::address() const
{
    return m_address;
}

QTime MacRow::age() const
{
    return m_age;
}

int MacRow::elapsed() const
{
    int age = m_age.elapsed();
    age /= 1e3;
    return age;
}

void MacRow::reset_age()
{
    m_age.restart();
}
