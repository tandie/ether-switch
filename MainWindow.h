#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include <iostream>

#include "MacTableThread.h"
#include "PortListThread.h"
#include "TimerDialog.h"

using Tins::PDU;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void display_row(const QString &address, const QString &port);
    void display_port(const QString &name, const bool &active);
    void port_down( int idx );
    void port_up( int idx );
    void hide_row( int idx );
    void flush_mac_table();

private slots:
    void on_actionSetMacTimer_triggered();
    void on_actionClearMacTable_triggered();

private:
    Ui::MainWindow *ui;
    TimerDialog *timer_dialog;
    MacTableThread *mac_table;
    PortListThread *ports;

    void full_width_table( QHeaderView *header );
    void start_sniffing();

};

#endif // MAINWINDOW_H
