#include "PortThread.h"

using Tins::PDU;
using Tins::PacketSender;
using Tins::Sniffer;
using Tins::SnifferConfiguration;
using Tins::NetworkInterface;
using Tins::EthernetII;
using Tins::Dot3;

PortThread::PortThread(QObject *parent)
    : QThread(parent)
{
    m_idle.start();
}

PortThread::PortThread(NetworkInterface port)
{
    m_port = port;
    m_idle.start();
    m_active = true;
    sender = PacketSender(m_port);
}

PortThread::~PortThread()
{
    mutex.lock();
    condition.wakeOne();
    mutex.unlock();

    wait();
}

void PortThread::run() {
    SnifferConfiguration config;
    config.set_direction(PCAP_D_IN);
    config.set_promisc_mode(true);

    try {
        QString src_mac;
        PDU *pdu;
        EthernetII eth_frame;
        Dot3 dot3_frame;
        Sniffer sniffer(m_port.name(), config);

        while (&m_port != NULL) {
            pdu = sniffer.next_packet();

            if (pdu != NULL) {

                mutex.lock();
                m_frame_queue.append(pdu);
                m_idle.restart();
                mutex.unlock();

                switch (pdu->pdu_type())
                {
                    case PDU::ETHERNET_II:
                        eth_frame = pdu->rfind_pdu<EthernetII>();
                        src_mac = QString::fromStdString( eth_frame.src_addr().to_string() );
                        emit find_entry(src_mac, name());
                    break;

                    case PDU::DOT3:
                        dot3_frame = pdu->rfind_pdu<Dot3>();
                        src_mac = QString::fromStdString( dot3_frame.src_addr().to_string() );
                        emit find_entry(src_mac, name());

                    default:
                        continue;
                    break;
                }
            }
        }

    } catch (socket_open_error e) {
        std::cout << "Chyba pri otvarani soketu" << std::endl;
    } catch (pcap_error e) {
        std::cout << "Port " << m_port.name() << " je v stave shutdown" << std::endl;
    }
}

void PortThread::send(PDU &pdu)
{
    mutex.lock();
    sender.send(pdu);
    mutex.unlock();
}

QString PortThread::name() const
{
    return QString::fromStdString( m_port.name() );
}

QList<PDU*> PortThread::frame_queue()
{
    mutex.lock();
    QList<PDU*> old_queue = m_frame_queue;
    m_frame_queue.clear();
    mutex.unlock();
    return old_queue;
}

int PortThread::idle() const
{
    int time = m_idle.elapsed();
    time /= 1e3;
    return time;
}

bool PortThread::active() const
{
    return m_active;
}
