#ifndef PORTTHREAD_H
#define PORTTHREAD_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QList>
#include <QTime>
#include <tins/network_interface.h>
#include <tins/packet_sender.h>
#include <tins/sniffer.h>
#include <tins/ethernetII.h>
#include <tins/dot3.h>
#include <tins/pdu.h>
#include <iostream>

using Tins::NetworkInterface;
using Tins::PacketSender;
using Tins::Sniffer;
using Tins::SnifferConfiguration;
using Tins::PDU;
using Tins::EthernetII;
using Tins::socket_open_error;
using Tins::pcap_error;

class PortThread : public QThread
{
    Q_OBJECT

public:
    PortThread(QObject *parent = 0);
    ~PortThread();
    PortThread(NetworkInterface port);

    QString name() const;
    QString state() const;
    QList<PDU*> frame_queue();
    bool active() const;
    void send(PDU &pdu);
    int idle() const;

signals:
     void find_entry(const QString &address, const QString &port);

protected:
     void run() override;

private:
     QMutex mutex;
     QWaitCondition condition;
     PacketSender sender;
     NetworkInterface m_port;
     QList<PDU*> m_frame_queue;
     QTime m_idle;
     bool m_active;
};

#endif // PORTTHREAD_H
