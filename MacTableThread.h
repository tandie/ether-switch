#ifndef MACTABLETHREAD_H
#define MACTABLETHREAD_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QList>
#include <MacRow.h>
#include <tins/pdu.h>
#include <iostream>

using Tins::PDU;

class MacTableThread : public QThread
{
    Q_OBJECT

public:
    MacTableThread(QObject *parent = 0);
    ~MacTableThread();

    void add(const QString &address, const QString &port);
    void clear();
    void timer(int seconds);
    int timer();
    void remove(int idx);

public slots:
    void find_entry(const QString &address, const QString &port);
    QString find_dest(const QString &dst_mac) const;
    void set_mac_timer(int seconds);

signals:
    void display_row(const QString &address, const QString &port);
    void hide_row(int idx);
    void flush_mac_table();

protected:
    void run();

private:
    QList<MacRow*> rows;
    QMutex mutex;
    QWaitCondition condition;
    int m_timer;

    bool abort;

};

#endif // MACTABLETHREAD_H
