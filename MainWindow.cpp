#include "MainWindow.h"
#include <ui_MainWindow.h>

using namespace Tins;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    mac_table = new MacTableThread();
    ports = new PortListThread();
    timer_dialog = new TimerDialog();

    full_width_table(ui->mac_table->horizontalHeader());
    full_width_table(ui->ports->horizontalHeader());

    connect(mac_table, &MacTableThread::display_row, this, &MainWindow::display_row);
    connect(mac_table, &MacTableThread::hide_row, this, &MainWindow::hide_row);
    connect(mac_table, &MacTableThread::flush_mac_table, this, &MainWindow::flush_mac_table);
    connect(ports, &PortListThread::display_port,this, &MainWindow::display_port);
    connect(ports, &PortListThread::find_dest, mac_table, &MacTableThread::find_dest, Qt::BlockingQueuedConnection);
    connect(timer_dialog, &TimerDialog::set_mac_timer, mac_table, &MacTableThread::set_mac_timer);
    connect(ports, &PortListThread::port_down, this, &MainWindow::port_down);
    connect(ports, &PortListThread::port_up, this, &MainWindow::port_up);

    ports->refresh();
    start_sniffing();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::start_sniffing()
{
    foreach (PortThread *port, ports->get_ports() ) {
        port->start();
        connect(port, &PortThread::find_entry, mac_table, &MacTableThread::find_entry);
    }
    ports->start();
    mac_table->start();
}

void MainWindow::display_row(const QString &address, const QString &port) {
    int row_count = ui->mac_table->rowCount();
    ui->mac_table->insertRow( row_count );
    ui->mac_table->setItem( row_count, 0, new QTableWidgetItem( address ) );
    ui->mac_table->setItem( row_count, 1, new QTableWidgetItem( port ) );
}

void MainWindow::display_port(const QString &name, const bool &active)
{
    int row_count = ui->ports->rowCount();
    QString state;
    ui->ports->insertRow( row_count );
    ui->ports->setItem( row_count, 0, new QTableWidgetItem( name ) );

    if (active) {
        state = "Aktívny";
    } else {
        state = "Neaktívny";
    }

    ui->ports->setItem( row_count, 1, new QTableWidgetItem( state ) );
}

void MainWindow::hide_row(int idx) {
    ui->mac_table->removeRow(idx);
}

void MainWindow::flush_mac_table() {
    for( int i = ui->mac_table->rowCount(); ui->mac_table->rowCount() > 0; i-- ) {
        ui->mac_table->removeRow( i );
    }
}

void MainWindow::port_down(int idx)
{
    QTableWidgetItem *item = ui->ports->item(idx, 1);
    item->setText("Neaktívny");
}

void MainWindow::port_up(int idx)
{
    QTableWidgetItem *item = ui->ports->item(idx, 1);
    item->setText("Aktívny");
}

void MainWindow::on_actionSetMacTimer_triggered()
{
    timer_dialog->show();
}

void MainWindow::on_actionClearMacTable_triggered()
{
    mac_table->clear();
}

void MainWindow::full_width_table( QHeaderView *header )
{
    for( int i = 0; i < header->count(); ++i ) {
        header->setSectionResizeMode(i, QHeaderView::Stretch);
    }
}
