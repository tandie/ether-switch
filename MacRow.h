#ifndef MACROW_H
#define MACROW_H

#include <QTime>

class MacRow
{
public:
    MacRow();
    MacRow(const MacRow &other);
    ~MacRow();

    MacRow(const QString &address, const QString &port);

    QString address() const;
    QString port() const;
    QTime age() const;

    void reset_age();
    int elapsed() const;

private:
    QString m_address;
    QString m_port;
    QTime m_age;
};

#endif // MACROW_H
