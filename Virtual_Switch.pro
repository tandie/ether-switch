#-------------------------------------------------
#
# Project created by QtCreator 2017-10-05T21:26:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Virtual_Switch
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    TimerDialog.cpp \
    MacRow.cpp \
    PortThread.cpp \
    MacTableThread.cpp \
    PortListThread.cpp

HEADERS  += MainWindow.h \
    TimerDialog.h \
    MacRow.h \
    PortThread.h \
    MacTableThread.h \
    PortListThread.h

FORMS    += MainWindow.ui \
    TimerDialog.ui

LIBS += -ltins
