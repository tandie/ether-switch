#include "TimerDialog.h"
#include "ui_TimerDialog.h"

TimerDialog::TimerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TimerDialog)
{
    ui->setupUi(this);
}

TimerDialog::~TimerDialog()
{
    delete ui;
}

void TimerDialog::on_cancelBtn_clicked()
{
    this->close();
}

void TimerDialog::on_setBtn_clicked()
{
    int timer = ui->timerEdit->text().toInt();
    emit set_mac_timer( timer );
    this->close();
}
