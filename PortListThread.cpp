#include "PortListThread.h"

using Tins::NetworkInterface;
using Tins::PDU;
using Tins::EthernetII;

PortListThread::PortListThread(QObject *parent)
    : QThread(parent)
{

}


PortListThread::~PortListThread()
{
    mutex.lock();
    condition.wakeOne();
    mutex.unlock();

    wait();
}

void PortListThread::run()
{
    QList<PDU*> frame_queue;

    while (true) {
        for (int idx = 0; idx < ports.size(); idx++) {
            PortThread *port = ports.at(idx);

            if (port->idle() >= 5) {
                emit port_down(idx);
            } else {
                emit port_up(idx);
            }

            frame_queue = port->frame_queue();

            foreach ( PDU *pdu, frame_queue ) {
                if (pdu->pdu_type() == PDU::ETHERNET_II) {
                    EthernetII eth_frame = pdu->rfind_pdu<EthernetII>();
                    const QString dst_mac = QString::fromStdString( eth_frame.dst_addr().to_string() );
                    PDU &frame = *pdu;

                    mutex.lock();
                    QString dst_port = find_dest(dst_mac);
                    mutex.unlock();

                    if (dst_port == NULL) {
                        broadcast(port->name(), frame);
                    } else {
                        unicast(dst_port, frame);
                    }
                }
            }
        }
    }
}

PortThread* PortListThread::find(const QString &port_name)
{
    foreach(PortThread *port, ports) {
        if (!port->name().compare(port_name)) {
            return port;
        }
    }
    return NULL;
}

void PortListThread::refresh()
{
    ports.clear();

    foreach ( NetworkInterface port, NetworkInterface::all() ) {
        if (!port.is_loopback() && port.is_up()) {
            PortThread *m_port = new PortThread(port);
            mutex.lock();
            ports.append( m_port );
            mutex.unlock();
            emit display_port( m_port->name(), m_port->active() );
        }
    }
}

void PortListThread::unicast(const QString &dst_port, PDU &pdu)
{
    PortThread *port = find(dst_port);
    port->send(pdu);
}

void PortListThread::broadcast(const QString &src_port, PDU &pdu)
{
    foreach( PortThread *port, ports ) {
        if (port->name().compare(src_port) && port->active()) {
            port->send(pdu);
        }
    }
}

QList<PortThread *> PortListThread::get_ports() const
{
    return ports;
}
