#ifndef TIMERDIALOG_H
#define TIMERDIALOG_H

#include <QDialog>

namespace Ui {
class TimerDialog;
}

class TimerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TimerDialog(QWidget *parent = 0);
    ~TimerDialog();

signals:
    void set_mac_timer( int seconds );

private slots:
    void on_cancelBtn_clicked();
    void on_setBtn_clicked();

private:
    Ui::TimerDialog *ui;
};

#endif // TIMERDIALOG_H
